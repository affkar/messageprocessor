# MessageProcessor

This application processes messages that denote sales and produces a sales report and an adjustment report. 

Requires Maven and Java 8 to be installed and the JAVA_HOME and M2_HOME to be set appropriately to build.

Building:  
mvn clean install

Running: 
1. `cd` to the checked out project, 
2. build with `mvn clean install` and see that it has built fine with a success message.
3. then run with `java -jar target\messageprocessor-1.0-SNAPSHOT.jar`

Sample Run:
```$xslt
$ java -jar "C:\Users\KSU12\codeworkspace\messageprocessor\target\messageprocessor-1.0-SNAPSHOT.jar"
======Welcome to Message Processor====
Enter Messages to Process or Q to quit
1
Sale Not Accepted, because of an invalid format
possible valid formats are 1. <product> at <price>
2. <noofsales> sales of <product> at <price> each
3. <product> at <price>. <Adjustment-Add/Subtract/Multiply> <adjustmentprice> apple
Examples,
1. 20 sales of apple at 10p each
2. 30 sales of onion at 30p each
3. apple at 10p. Add ▒1 apple
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
Ok. Sale Accepted!
apple at 10p
============Summary of Sales============
1. productType='apple', noOfItems=10, TotalMoneyRealized= ▒ 1.00
============Summary of Sales End========

```
