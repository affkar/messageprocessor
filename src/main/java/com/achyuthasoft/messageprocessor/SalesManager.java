package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;
import com.achyuthasoft.messageprocessor.message.Sale;

import java.util.ArrayList;
import java.util.HashMap;

public class SalesManager {

    private SalesSummary salesSummary=new SalesSummary(new ArrayList<>(), new HashMap<>());
    private AdjustmentSummary adjustmentSummary=new AdjustmentSummary(new ArrayList<>());


    public void recordSale(Sale sale) {
        salesSummary.update(sale);
    }

    public void printSummaryOfSales(){
        salesSummary.print();
    }



    public void recordAdjustment(Adjustment adjustment) {
        adjustmentSummary.record(adjustment);
        salesSummary.update(adjustment);
    }

    public void printSummaryOfAdjustments(){
        adjustmentSummary.print();
    }

    public SalesSummaryView getSummaryOfSales() {
        return salesSummary;
    }

    public AdjustmentSummaryView getSummaryOfAdjustments() {
        return adjustmentSummary;
    }
}
