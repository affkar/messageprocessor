package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;
import com.achyuthasoft.messageprocessor.message.Sale;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class SalesSummary implements SalesSummaryView{

    private List<Sale> sales;
    private Map<String, SalesSummaryItem> productType_salesSummaryItemMap;

    public SalesSummary(List<Sale> sales, Map<String, SalesSummaryItem> productType_salesSummaryItemMap) {
        this.sales = sales;
        this.productType_salesSummaryItemMap = productType_salesSummaryItemMap;
    }

    public void update(Sale sale) {
        sales.add(sale);
        SalesSummaryItem existingSalesSummaryItem = productType_salesSummaryItemMap.get(sale.getProductType());
        if(existingSalesSummaryItem ==null){
            productType_salesSummaryItemMap.put(sale.getProductType(), new SalesSummaryItem(sale.getProductType(),1,sale.getPriceInPounds()));
        }else{
            productType_salesSummaryItemMap.put(sale.getProductType(), new SalesSummaryItem(sale.getProductType(),existingSalesSummaryItem.getNoOfItems() + 1,existingSalesSummaryItem.getPricePerItem()));
        }
    }

    public void print(){
        System.out.println("============Summary of Sales============");
        int i=1;
        for (SalesSummaryItem salesSummaryItem: productType_salesSummaryItemMap.values()) {
            System.out.println(""+ (i++) +". " + salesSummaryItem);
        }
        System.out.println("============Summary of Sales End========");
    }

    public void update(Adjustment adjustment) {
        SalesSummaryItem existingSalesSummaryItem = productType_salesSummaryItemMap.get(adjustment.getProductType());
        BigDecimal existingPricePerItem=existingSalesSummaryItem.getPricePerItem();
        BigDecimal newPricePerItem = adjustment.getAdjustmentOperation().getNewPricePerItem(existingPricePerItem, adjustment.getAdjustmentInPounds());
        productType_salesSummaryItemMap.put(adjustment.getProductType(),new SalesSummaryItem(existingSalesSummaryItem.getProductType(),existingSalesSummaryItem.getNoOfItems(),newPricePerItem));
    }

    @Override
    public Map<String, SalesSummaryItem> getProductTypeSalesSummaryItemMap() {
        return Collections.unmodifiableMap(productType_salesSummaryItemMap);
    }
}
