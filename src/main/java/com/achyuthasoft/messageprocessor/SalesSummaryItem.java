package com.achyuthasoft.messageprocessor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public final class SalesSummaryItem {
    private final String productType;
    private final Integer noOfItems;
    private final BigDecimal pricePerItem;

    public SalesSummaryItem(String productType, Integer noOfItems, BigDecimal pricePerItem) {
        this.productType = productType;
        this.noOfItems = noOfItems;
        this.pricePerItem=pricePerItem.setScale(2, RoundingMode.HALF_UP);
    }

    public String getProductType() {
        return productType;
    }

    public Integer getNoOfItems() {
        return noOfItems;
    }

    public BigDecimal getTotalMoneyRealized() {
        return pricePerItem.multiply(new BigDecimal(noOfItems));
    }

    public BigDecimal getPricePerItem() {
        return pricePerItem;
    }

    @Override
    public String toString() {
        return
                "productType='" + productType + '\'' +
                ", noOfItems=" + noOfItems +
                ", TotalMoneyRealized= £ " + getTotalMoneyRealized();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SalesSummaryItem)) return false;
        SalesSummaryItem that = (SalesSummaryItem) o;
        return Objects.equals(productType, that.productType) &&
                Objects.equals(noOfItems, that.noOfItems) &&
                Objects.equals(pricePerItem, that.pricePerItem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, noOfItems, pricePerItem);
    }
}
