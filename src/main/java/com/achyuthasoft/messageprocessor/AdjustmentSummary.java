package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;

import java.util.Collections;
import java.util.List;

public class AdjustmentSummary implements AdjustmentSummaryView{

    private List<Adjustment> adjustmentList;

    public AdjustmentSummary(List<Adjustment> adjustmentList) {
        this.adjustmentList = adjustmentList;
    }

    public void record(Adjustment adjustment) {
        adjustmentList.add(adjustment);
    }

    public void print() {
        System.out.println("==========Adjustment Summary==========");
        int i=1;
        for (Adjustment adjustment: adjustmentList) {
            System.out.println(""+i+". " + adjustment);
            i++;
        }
        System.out.println("==========Adjustment Summary End======");
    }

    @Override
    public List<Adjustment> getAdjustmentList(){
        return Collections.unmodifiableList(adjustmentList);
    }
}
