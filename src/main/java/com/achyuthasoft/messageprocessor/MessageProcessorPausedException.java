package com.achyuthasoft.messageprocessor;

public class MessageProcessorPausedException extends RuntimeException {
    public MessageProcessorPausedException() {
    }

    public MessageProcessorPausedException(String message) {
        super(message);
    }

    public MessageProcessorPausedException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageProcessorPausedException(Throwable cause) {
        super(cause);
    }

    public MessageProcessorPausedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

