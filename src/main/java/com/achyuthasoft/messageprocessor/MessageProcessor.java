package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.MessageType1;
import com.achyuthasoft.messageprocessor.message.MessageType2;
import com.achyuthasoft.messageprocessor.message.MessageType3;

import java.util.ArrayList;
import java.util.List;

public class MessageProcessor {

    private List<String> processedMessages=new ArrayList<>();
    private List<String> messagesYetToBeProcessed=new ArrayList<>();
    private SalesManager salesManager;

    MessageProcessor() {
    }

    public MessageProcessor(SalesManager salesManager) {
        this.salesManager = salesManager;
    }

    /**
     *
     * @param rawMessage
     * throws InvalidFormatException in case the message was in an unparseable format.
     * throws MessageProcessorPausedException in case the processor is paused as 50 messages have been processed.
     */
    public void processRawMessage(String rawMessage) {
        try {
            if (getNumberOfMessagesProcessed() <= 49) {
                if (MessageType1.canParse(rawMessage)) {
                    MessageType1.parseActionFromRawMessage(rawMessage).performActionWith(salesManager);
                } else if (MessageType2.canParse(rawMessage)) {
                    MessageType2.parseFromRawMessage(rawMessage).performActionWith(salesManager);
                } else if (MessageType3.canParse(rawMessage)) {
                    MessageType3.parseFromRawMessage(rawMessage).performActionWith(salesManager);
                } else {
                     throw new InvalidFormatException("Can't process message. Invalid Format");
                }
                processedMessages.add(rawMessage);
                if (getNumberOfMessagesProcessed() % 10 == 0) {
                    salesManager.printSummaryOfSales();
                }
                if (getNumberOfMessagesProcessed() == 50) {
                    System.out.println("Have Processed 50 Messages so far. So MessageProcessor pausing now ");
                    salesManager.printSummaryOfAdjustments();
                }
            } else {
                messagesYetToBeProcessed.add(rawMessage);
                throw new MessageProcessorPausedException();
            }
        }catch(IllegalArgumentException e){
            throw new InvalidFormatException("Can't process message",e);
        }
    }

    public List<String> getListOfProcessedMessages() {
        return processedMessages;
    }

    public Integer getNumberOfMessagesProcessed() {
        return processedMessages.size();
    }

    public SalesManager getSalesManager() {
        return salesManager;
    }
}
