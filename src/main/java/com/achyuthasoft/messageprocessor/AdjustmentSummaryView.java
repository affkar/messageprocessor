package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;

import java.util.List;

public interface AdjustmentSummaryView {
    List<Adjustment> getAdjustmentList();
}
