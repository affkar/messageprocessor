package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.SalesManager;

public interface Action {

    public void performActionWith(SalesManager salesManager);

}
