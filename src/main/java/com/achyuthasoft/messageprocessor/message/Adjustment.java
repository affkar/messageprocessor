package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public final class Adjustment {

    private final String productType;
    private final BigDecimal adjustmentInPounds;
    private final AdjustmentOperation adjustmentOperation;

    public Adjustment(String productType, BigDecimal adjustmentInPounds, AdjustmentOperation adjustmentOperation) {
        this.productType = productType;
        this.adjustmentInPounds = adjustmentInPounds.setScale(2, RoundingMode.HALF_UP);
        this.adjustmentOperation = adjustmentOperation;
    }

    public String getProductType() {
        return productType;
    }

    public BigDecimal getAdjustmentInPounds() {
        return adjustmentInPounds;
    }

    public AdjustmentOperation getAdjustmentOperation() {
        return adjustmentOperation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Adjustment)) return false;
        Adjustment that = (Adjustment) o;
        return Objects.equals(productType, that.productType) &&
                Objects.equals(adjustmentInPounds, that.adjustmentInPounds) &&
                adjustmentOperation == that.adjustmentOperation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, adjustmentInPounds, adjustmentOperation);
    }

    @Override
    public String toString() {
        return
                "productType='" + productType + '\'' +
                ", adjustmentInPounds=" + adjustmentInPounds +
                ", adjustmentOperation=" + adjustmentOperation ;
    }
}
