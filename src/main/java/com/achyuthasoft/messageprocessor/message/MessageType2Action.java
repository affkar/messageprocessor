package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.SalesManager;

public class MessageType2Action implements Action{

    private MessageType2 messageType2;

    public MessageType2Action(MessageType2 messageType2) {
        this.messageType2 = messageType2;
    }

    @Override
    public void performActionWith(SalesManager salesManager) {
        for(int i=0;i< messageType2.getNumberOfTimesTheSaleWasMade();i++) {
            salesManager.recordSale(messageType2.getSale());
        }
    }

    public MessageType2 getMessageType2() {
        return messageType2;
    }
}
