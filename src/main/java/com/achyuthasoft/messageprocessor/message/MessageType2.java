package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.achyuthasoft.messageprocessor.message.PriceStringUtil.getPriceFromString;

public class MessageType2 {
    public static final Pattern MESSAGETYPE2_RAWMESSAGE_PATTERN = Pattern.compile("(.*) sales of (.*) at (.*) each");
    private final Sale sale;
    private final Integer noOfTimesTheSaleWasMade;

    public MessageType2(Sale sale, Integer noOfTimesTheSaleWasMade) {
        this.sale=sale;
        this.noOfTimesTheSaleWasMade=noOfTimesTheSaleWasMade;
    }

    public static boolean canParse(String rawMessage){
        return MESSAGETYPE2_RAWMESSAGE_PATTERN.matcher(rawMessage).find();
    }

    public static Action parseFromRawMessage(String rawMessage) {
        Matcher matcher = MESSAGETYPE2_RAWMESSAGE_PATTERN.matcher(rawMessage);
        if(matcher.find()){
            String noOfTimesTheSaleWasMadeString = matcher.group(1).trim();
            String productString = matcher.group(2).trim();
            String priceString= matcher.group(3).trim();
            BigDecimal price;
            Integer noOfTimesTheSaleWasMade=Integer.valueOf(noOfTimesTheSaleWasMadeString);
            price = getPriceFromString(priceString);
            return new MessageType2(new Sale(productString.toLowerCase(),price), noOfTimesTheSaleWasMade).getAction();
        }else{
            throw new IllegalArgumentException(String.format("Not a valid input. %s. Should confirm to Regex %s", rawMessage, MESSAGETYPE2_RAWMESSAGE_PATTERN));
        }
    }

    public Sale getSale() {
        return sale;
    }

    public Integer getNumberOfTimesTheSaleWasMade() {
        return noOfTimesTheSaleWasMade;
    }

    public MessageType2Action getAction(){
        return new MessageType2Action(this);
    }
}
