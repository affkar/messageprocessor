package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.SalesManager;

public class MessageType1Action implements Action{

    private MessageType1 messageType1;

    public MessageType1Action(MessageType1 messageType1) {
        this.messageType1 = messageType1;
    }

    @Override
    public void performActionWith(SalesManager salesManager) {
        salesManager.recordSale(messageType1.getSale());
    }

    public MessageType1 getMessageType1() {
        return messageType1;
    }
}
