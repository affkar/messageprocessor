package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.achyuthasoft.messageprocessor.message.PriceStringUtil.getPriceFromString;

public class MessageType3 {
    public static final Pattern MESSAGETYPE3_RAWMESSAGE_PATTERN = Pattern.compile("(.*) at (.*)\\. ([^ ]*?) (.*) (.*)");
    private final Sale sale;
    private final Adjustment adjustment;

    public MessageType3(Sale sale, Adjustment adjustment) {
        this.sale=sale;
        this.adjustment=adjustment;
    }

    public Adjustment getAdjustment() {
        return adjustment;
    }

    public Sale getSale() {
        return sale;
    }

    public static boolean canParse(String rawMessage){
        return MESSAGETYPE3_RAWMESSAGE_PATTERN.matcher(rawMessage).find();
    }

    public static Action parseFromRawMessage(String rawMessage) {
        Matcher matcher = MESSAGETYPE3_RAWMESSAGE_PATTERN.matcher(rawMessage);
        if(matcher.find()){
            String productString = matcher.group(1).trim();
            String priceString= matcher.group(2).trim();
            String adjustmentOperationString =matcher.group(3).trim();
            String adjustmentAmountString =matcher.group(4).trim();
            String adjustmentProductString =matcher.group(5).trim();
            if(!adjustmentProductString.equalsIgnoreCase(productString)){
                throw new IllegalArgumentException(String.format("adjustment product string [%s] should match product string [%s]",adjustmentProductString,productString));
            }
            BigDecimal price = getPriceFromString(priceString);
            BigDecimal adjustmentAmount= getPriceFromString(adjustmentAmountString);
            AdjustmentOperation adjustmentOperation=AdjustmentOperation.valueOf(adjustmentOperationString);
            return new MessageType3(new Sale(productString.toLowerCase(),price), new Adjustment(adjustmentProductString,adjustmentAmount, adjustmentOperation)).getAction();
        }else{
            throw new IllegalArgumentException(String.format("Not a valid input. %s. Should confirm to Regex %s", rawMessage, MESSAGETYPE3_RAWMESSAGE_PATTERN));
        }
    }

    private Action getAction() {
        return new MessageType3Action(this);
    }
}
