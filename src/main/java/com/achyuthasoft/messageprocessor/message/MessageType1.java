package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.achyuthasoft.messageprocessor.message.PriceStringUtil.getPriceFromString;

public class MessageType1{
    public static final Pattern MESSAGETYPE1_RAWMESSAGE_PATTERN = Pattern.compile("(.*) at (.*)");
    private final Sale sale;

    public MessageType1(Sale sale) {
        this.sale=sale;
    }

    public static boolean canParse(String rawMessage){
        return !(rawMessage.contains("each") || rawMessage.contains("of") || rawMessage.contains("Add") || rawMessage.contains("Subtract") || rawMessage.contains("Multiply")) && MESSAGETYPE1_RAWMESSAGE_PATTERN.matcher(rawMessage).find();
    }

    public static Action parseActionFromRawMessage(String rawMessage) {
        if(canParse(rawMessage)) {
            Matcher matcher = MESSAGETYPE1_RAWMESSAGE_PATTERN.matcher(rawMessage);
            if (matcher.find()) {
                String productString = matcher.group(1).trim();
                String priceString = matcher.group(2).trim();
                BigDecimal price = getPriceFromString(priceString);
                return new MessageType1(new Sale(productString.toLowerCase(), price)).getAction();
            }else{
                throw new IllegalArgumentException(String.format("Not a valid input. %s. Should not contain the words [each] and [of] and confirm to Regex %s", rawMessage, MESSAGETYPE1_RAWMESSAGE_PATTERN));
            }
        }else{
            throw new IllegalArgumentException(String.format("Not a valid input. %s. Should not contain the words [each] and [of] and confirm to Regex %s", rawMessage, MESSAGETYPE1_RAWMESSAGE_PATTERN));
        }
    }

    public Sale getSale() {
        return sale;
    }

    public MessageType1Action getAction(){
        return new MessageType1Action(this);
    }
}
