package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;

public enum AdjustmentOperation {
    Add("Add"), Subtract("Subtract"), Multiply("Multiply");

    String asString;
    AdjustmentOperation(String asString){
        this.asString=asString;
    }

    public BigDecimal getNewPricePerItem(BigDecimal existingPricePerItem, BigDecimal adjustmentFactor){
        switch(this){
            case Add:
                return existingPricePerItem.add(adjustmentFactor);
            case Subtract:
                return existingPricePerItem.subtract(adjustmentFactor);
            case Multiply:
                return existingPricePerItem.multiply(adjustmentFactor);
            default:
                return existingPricePerItem;
        }
    }


}
