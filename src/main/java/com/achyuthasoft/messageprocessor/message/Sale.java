package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class Sale {
    private String productType;
    private BigDecimal priceInPounds;

    public Sale(String productType, BigDecimal priceInPounds) {
        this.productType = productType;
        this.priceInPounds = priceInPounds.setScale(2, RoundingMode.HALF_UP);
    }

    public String getProductType() {
        return productType;
    }

    public BigDecimal getPriceInPounds() {
        return priceInPounds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sale)) return false;
        Sale sale = (Sale) o;
        return Objects.equals(productType, sale.productType) &&
                Objects.equals(priceInPounds, sale.priceInPounds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productType, priceInPounds);
    }

    @Override
    public String toString() {
        return "Sale{" +
                "productType='" + productType + '\'' +
                ", priceInPounds=" + priceInPounds +
                '}';
    }
}
