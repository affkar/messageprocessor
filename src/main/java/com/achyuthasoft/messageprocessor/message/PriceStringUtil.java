package com.achyuthasoft.messageprocessor.message;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PriceStringUtil {

    public static BigDecimal getPriceFromString(String priceString) {
        BigDecimal price;
        if(priceString.endsWith("p")){
            price=new BigDecimal(priceString.substring(0,priceString.length()-1).trim()).divide(new BigDecimal(100));
        }else if(priceString.endsWith("£")){
            price=new BigDecimal(priceString.substring(0,priceString.length()-1).trim());
        }else if(priceString.startsWith("£")){
            price=new BigDecimal(priceString.substring(1,priceString.length()).trim());
        }else{
            throw new IllegalArgumentException("Expected either £ at the start or end or p at the end for price seqment but was "+priceString);
        }
        return price.setScale(2,RoundingMode.HALF_UP);
    }
}
