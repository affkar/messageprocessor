package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.SalesManager;

public class MessageType3Action implements Action {
    private MessageType3 messageType3;

    public MessageType3Action(MessageType3 messageType3) {
        this.messageType3 = messageType3;
    }

    @Override
    public void performActionWith(SalesManager salesManager) {
        salesManager.recordSale(messageType3.getSale());
        salesManager.recordAdjustment(messageType3.getAdjustment());
    }

    public MessageType3 getMessageType3() {
        return messageType3;
    }
}
