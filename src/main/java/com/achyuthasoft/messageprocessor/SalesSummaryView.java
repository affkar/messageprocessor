package com.achyuthasoft.messageprocessor;

import java.util.Map;

public interface SalesSummaryView {

    Map<String, SalesSummaryItem> getProductTypeSalesSummaryItemMap();

}
