package com.achyuthasoft.messageprocessor;

import java.util.Scanner;

public class Application {

    private static MessageProcessor messageProcessor;

    public static void main(String[] args) {
        messageProcessor=new MessageProcessor(new SalesManager());
        System.out.println("======Welcome to Message Processor====");
        System.out.println("Enter Messages to Process or Q to quit");
        Scanner scanner=new Scanner(System.in);
        String line;
        while(!(line=scanner.nextLine()).equalsIgnoreCase("Q")){
            String rawMessage=line;
            try {
                messageProcessor.processRawMessage(rawMessage);
                System.out.println("Ok. Sale Accepted!");
            } catch(MessageProcessorPausedException ife){
                System.out.println("Message Processing Paused. Can't process messages right now!");
            } catch(InvalidFormatException ife){
                System.out.println("Sale Not Accepted, because of an invalid format");
                System.out.println("possible valid formats are " +
                        "1. <product> at <price> \n" +
                        "2. <noofsales> sales of <product> at <price> each \n" +
                        "3. <product> at <price>. <Adjustment-Add/Subtract/Multiply> <adjustmentprice> apple \n" +
                        "Examples, \n" +
                        "1. 20 sales of apple at 10p each\n" +
                        "2. 30 sales of onion at 30p each\n" +
                        "3. apple at 10p. Add £1 apple");

            }
        }

    }

}
