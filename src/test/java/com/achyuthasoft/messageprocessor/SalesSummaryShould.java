package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;
import com.achyuthasoft.messageprocessor.message.AdjustmentOperation;
import com.achyuthasoft.messageprocessor.message.Sale;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SalesSummaryShould {

    private SalesSummary salesSummary;

    @Mock
    private List<Sale> sales;

    @Mock
    private Map<String, SalesSummaryItem> productType_salesSummaryItemMap;

    @Captor
    ArgumentCaptor<SalesSummaryItem> salesSummaryItem;


    @Before
    public void setUp() throws Exception {
        salesSummary=new SalesSummary(sales,productType_salesSummaryItemMap);
    }

    @Test
    public void updateSales() {
        Sale appleSale = new Sale("apple", new BigDecimal(.20f));

        salesSummary.update(appleSale);

        verify(sales, times(1)).add(appleSale);
        verify(productType_salesSummaryItemMap, times(1)).put(eq("apple"),salesSummaryItem.capture());
        assertEquals(new SalesSummaryItem("apple", 1, new BigDecimal(.20f)), salesSummaryItem.getValue());
    }

    @Test
    public void updateExistingSales() {
        when(productType_salesSummaryItemMap.get("apple")).thenReturn(new SalesSummaryItem("apple", 1, new BigDecimal(.20f)));
        Sale appleSale = new Sale("apple", new BigDecimal(.20f));

        salesSummary.update(appleSale);

        verify(sales, times(1)).add(appleSale);
        verify(productType_salesSummaryItemMap, times(1)).put(eq("apple"),salesSummaryItem.capture());
        assertEquals(new SalesSummaryItem("apple", 2, new BigDecimal(.20f)), salesSummaryItem.getValue());
    }

    @Test
    public void updateSalesWhenThereAreAdjustments() {
        when(productType_salesSummaryItemMap.get("apple")).thenReturn(new SalesSummaryItem("apple", 2, new BigDecimal(.20f)));
        Adjustment adjustment=new Adjustment("apple", new BigDecimal(0.1f), AdjustmentOperation.Add);

        salesSummary.update(adjustment);

        verify(productType_salesSummaryItemMap, times(1)).put(eq("apple"),salesSummaryItem.capture());
        assertEquals(new SalesSummaryItem("apple", 2, new BigDecimal(.30f)), salesSummaryItem.getValue());
    }

}
