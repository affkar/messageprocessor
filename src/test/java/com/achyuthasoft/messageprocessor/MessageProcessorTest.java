package com.achyuthasoft.messageprocessor;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static com.achyuthasoft.messageprocessor.MessageProcessorShould.*;
import static org.junit.Assert.assertEquals;

public class MessageProcessorTest {

    private MessageProcessor messageProcessor;
    private SalesManager salesManager;

    @Rule
    public ExpectedException expectedException=ExpectedException.none();

    @Before
    public void setup(){
        salesManager=new SalesManager();
        messageProcessor =new MessageProcessor(salesManager);
    }

    @Test
    public void testSalesSummary(){
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        assertEquals(new SalesSummaryItem("apple", 105, new BigDecimal( .10)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        assertEquals(new SalesSummaryItem("apple", 153, new BigDecimal(0.30)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
    }

    @Test
    public void testSalesSummary_MessageType1_OneType_NoAdjustments(){
        IntStream.range(1,51).forEach(it-> messageProcessor.processRawMessage(APPLE_AT_10_P));
        assertEquals(new SalesSummaryItem("apple", 50, new BigDecimal(.10)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
    }

    @Test
    public void testSalesSummary_MessageType1_TwoTypes_NoAdjustments(){
        IntStream.range(1,26).forEach(it-> { messageProcessor.processRawMessage(APPLE_AT_10_P);
            messageProcessor.processRawMessage(ORANGE_AT_30_P); });
        assertEquals(new SalesSummaryItem("apple", 25, new BigDecimal(.10F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
        assertEquals(new SalesSummaryItem("orange", 25, new BigDecimal(.30F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("orange"));
    }

    @Test
    public void testSalesSummary_MessageType2_OneType_NoAdjustments(){
        IntStream.range(1,51).forEach(it-> { /*messageProcessor.processRawMessage(APPLE_AT_10_P);*/messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH); });
        assertEquals(new SalesSummaryItem("apple", 1000, new BigDecimal(.10F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
//        assertEquals(new SalesSummaryItem("orange", 50, .30F), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("orange"));
    }

    @Test
    public void testSalesSummary_MessageType2_TwoTypes_NoAdjustments(){
        expectedException.expect(MessageProcessorPausedException.class);
        IntStream.range(1,27).forEach(it-> { messageProcessor.processRawMessage(_30_SALES_OF_ONION_AT_30_P_EACH);
            messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH); });
        assertEquals(new SalesSummaryItem("apple",500, new BigDecimal(.10F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
        assertEquals(new SalesSummaryItem("onion", 750, new BigDecimal(.30F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("onion"));
    }

    @Test
    public void testSalesSummary_MessageType3_OneType_WithAdjustments(){
        IntStream.range(1,51).forEach(it-> { messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE); });
        assertEquals(new SalesSummaryItem("apple", 50, new BigDecimal(10.10F)), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
//        assertEquals(new SalesSummaryItem("onion", 1500, .30F), messageProcessor.getSalesManager().getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("onion"));
    }

}
