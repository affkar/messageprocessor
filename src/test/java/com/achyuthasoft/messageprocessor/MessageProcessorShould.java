package com.achyuthasoft.messageprocessor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static com.achyuthasoft.hamcrestmatcher.CollectionEquals.equalTo;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MessageProcessorShould {

    public static final String APPLE_AT_10_P = "apple at 10p";
    public static final String _20_SALES_OF_APPLE_AT_10_P_EACH = "20 sales of apple at 10p each";
    public static final String _30_SALES_OF_ONION_AT_30_P_EACH = "30 sales of onion at 30p each";
    public static final String APPLE_AT_10_P_ADD_20_P_APPLE = "apple at 10p. Add 20p apple";
    public static final String ORANGE_AT_30_P = "Orange at 30p";
    public static final String CHICKEN_AT_80_P = "Chicken at 80p";
    public static final String PLAIN_FLOUR_AT_90_P = "Plain Flour at 90p";
    public static final String ONION_AT_POUND_1_30 = "Onion at £ 1.30 ";

    @InjectMocks
    MessageProcessor messageProcessor;

    @Mock
    SalesManager salesManager;

    @Test
    public void processRawMessagesAndGetAListOfMessagesThatItProcessed(){
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE);
        assertThat(messageProcessor.getListOfProcessedMessages(), equalTo(Arrays.asList(APPLE_AT_10_P, _20_SALES_OF_APPLE_AT_10_P_EACH, APPLE_AT_10_P_ADD_20_P_APPLE)));
    }

    @Test
    public void processRawMessagesAndReturnTheNumberOfMessagesThatItProcessed(){
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE);
        assertEquals(new Integer(3), messageProcessor.getNumberOfMessagesProcessed());
    }

    @Test
    public void process11RawMessagesAndCheckReportWasPrinted(){
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE);
        messageProcessor.processRawMessage(ORANGE_AT_30_P);
        messageProcessor.processRawMessage(CHICKEN_AT_80_P);
        messageProcessor.processRawMessage(PLAIN_FLOUR_AT_90_P);
        messageProcessor.processRawMessage(APPLE_AT_10_P);
        messageProcessor.processRawMessage(_20_SALES_OF_APPLE_AT_10_P_EACH);
        messageProcessor.processRawMessage(APPLE_AT_10_P_ADD_20_P_APPLE);
        messageProcessor.processRawMessage(ORANGE_AT_30_P);
        messageProcessor.processRawMessage(CHICKEN_AT_80_P);
        assertThat(messageProcessor.getListOfProcessedMessages(), equalTo(Arrays.asList(
                APPLE_AT_10_P, _20_SALES_OF_APPLE_AT_10_P_EACH, APPLE_AT_10_P_ADD_20_P_APPLE,ORANGE_AT_30_P,CHICKEN_AT_80_P, PLAIN_FLOUR_AT_90_P,
                APPLE_AT_10_P, _20_SALES_OF_APPLE_AT_10_P_EACH, APPLE_AT_10_P_ADD_20_P_APPLE,ORANGE_AT_30_P,CHICKEN_AT_80_P
        )));
        assertEquals(new Integer(11), messageProcessor.getNumberOfMessagesProcessed());
    }

    @Test
    public void haveASalesManager() {
        assertNotNull(messageProcessor.getSalesManager());
    }

}
