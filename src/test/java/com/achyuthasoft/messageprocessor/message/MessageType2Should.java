package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.MessageProcessorShould;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MessageType2Should {

    MessageType2 messageType2;

    Sale sale;

    Integer noOfTimesTheSaleWasMade;

    @Before
    public void setup(){
        sale=new Sale("Apples", new BigDecimal(2.0));
        noOfTimesTheSaleWasMade=20;
        messageType2=new MessageType2(sale,noOfTimesTheSaleWasMade);
    }
    @Test
    public void containASale(){
        assertNotNull(messageType2.getSale());
    }

    @Test
    public void containTheNumberOfTimesTheSaleWasMade(){
        assertNotNull(messageType2.getNumberOfTimesTheSaleWasMade());
    }

    @Test
    public void returnASaleThatItWasConstructedWith(){
        assertEquals(sale, messageType2.getSale());
    }

    @Test
    public void returnTheNumberOfTimesTheSaleWasMadeThatItWasConstructedWith(){
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());
    }

    @Test
    public void parseRawMessageToMessage(){
        MessageType2 messageType2 = ((MessageType2Action)MessageType2.parseFromRawMessage(MessageProcessorShould._20_SALES_OF_APPLE_AT_10_P_EACH)).getMessageType2();
        assertEquals(new Sale("apple", new BigDecimal(.10f)), messageType2.getSale());
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());

        messageType2 = ((MessageType2Action)MessageType2.parseFromRawMessage("20 sales of apple at 10 p each")).getMessageType2();
        assertEquals(new Sale("apple", new BigDecimal(.10f)), messageType2.getSale());
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());

        messageType2 = ((MessageType2Action)MessageType2.parseFromRawMessage("20 sales of apple at 10 £ each")).getMessageType2();
        assertEquals(new Sale("apple", new BigDecimal(10.00f)), messageType2.getSale());
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());

        messageType2 = ((MessageType2Action)MessageType2.parseFromRawMessage("20 sales of apple at £ 10  each")).getMessageType2();
        assertEquals(new Sale("apple", new BigDecimal(10.00f)), messageType2.getSale());
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());

        messageType2 = ((MessageType2Action)MessageType2.parseFromRawMessage("20 sales of apple at £ 10.20  each")).getMessageType2();
        assertEquals(new Sale("apple", new BigDecimal(10.20f)), messageType2.getSale());
        assertEquals(Integer.valueOf(20), messageType2.getNumberOfTimesTheSaleWasMade());

    }

}
