package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.MessageProcessorShould;
import org.hamcrest.text.MatchesPattern;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class MessageType1Should {

    @Rule
    public ExpectedException expectedException=ExpectedException.none();

    MessageType1 messageType1;

    Sale sale;

    @Before
    public void setup(){
        sale=new Sale("Apples", new BigDecimal(2.0));
        messageType1=new MessageType1(sale);
    }
    @Test
    public void containASale(){
        assertNotNull(messageType1.getSale());
    }

    @Test
    public void returnASaleThatItWasConstructedWith(){
        assertEquals(sale, messageType1.getSale());
    }

    @Test
    public void parseRawMessageToMessage(){
        MessageType1 messageType1 = ((MessageType1Action)MessageType1.parseActionFromRawMessage(MessageProcessorShould.APPLE_AT_10_P)).getMessageType1();
        assertEquals(new Sale("apple", new BigDecimal(.10)), messageType1.getSale());

        messageType1 = ((MessageType1Action)MessageType1.parseActionFromRawMessage(MessageProcessorShould.ORANGE_AT_30_P)).getMessageType1();
        assertEquals(new Sale("orange", new BigDecimal(.30)), messageType1.getSale());

        messageType1 = ((MessageType1Action)MessageType1.parseActionFromRawMessage(MessageProcessorShould.ONION_AT_POUND_1_30)).getMessageType1();
        assertEquals(new Sale("onion", new BigDecimal(1.30)), messageType1.getSale());

        messageType1 = ((MessageType1Action)MessageType1.parseActionFromRawMessage("Onion at £1.30 ")).getMessageType1();
        assertEquals(new Sale("onion", new BigDecimal(1.30)), messageType1.getSale());

    }

    @Test
    public void parseRawMessageToMessage_throwIllegalArgumentException(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(MatchesPattern.matchesPattern("Not a valid input.*"));

        MessageType1 messageType1 = ((MessageType1Action)MessageType1.parseActionFromRawMessage(MessageProcessorShould._20_SALES_OF_APPLE_AT_10_P_EACH)).getMessageType1();
        assertEquals(new Sale("apple", new BigDecimal(.10f)), messageType1.getSale());



    }





}
