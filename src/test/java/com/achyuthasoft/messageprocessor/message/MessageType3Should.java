package com.achyuthasoft.messageprocessor.message;

import com.achyuthasoft.messageprocessor.MessageProcessorShould;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MessageType3Should {

    public static final Sale APPLE_SALE = new Sale("Apples", new BigDecimal(0.2f));
    public static final Adjustment APPLE_ADJUSTMENT = new Adjustment("Apples", new BigDecimal(0.2f), AdjustmentOperation.Add);
    MessageType3 messageType3;

    @Before
    public void setUp(){
        messageType3=new MessageType3(APPLE_SALE, APPLE_ADJUSTMENT);
    }


    @Test
    public void returnAdjustmentThatWasSetDuringConstruction(){
        assertEquals(APPLE_ADJUSTMENT, messageType3.getAdjustment());
    }

    @Test
    public void returnSaleThatWasSetDuringConstruction(){
        assertEquals(APPLE_SALE, messageType3.getSale());
    }

    @Test
    public void parseRawMessageToMessage(){
        MessageType3 messageType3 = ((MessageType3Action)MessageType3.parseFromRawMessage(MessageProcessorShould.APPLE_AT_10_P_ADD_20_P_APPLE)).getMessageType3();
        assertEquals(new Sale("apple", new BigDecimal(.10f)), messageType3.getSale());
        assertEquals(new Adjustment("apple",new BigDecimal(0.20f),AdjustmentOperation.Add), messageType3.getAdjustment());

        messageType3 = ((MessageType3Action)MessageType3.parseFromRawMessage("apple at 10 £. Add 20 £ apple")).getMessageType3();
        assertEquals(new Sale("apple", new BigDecimal(10)), messageType3.getSale());
        assertEquals(new Adjustment("apple",new BigDecimal(20),AdjustmentOperation.Add), messageType3.getAdjustment());

        messageType3 = ((MessageType3Action)MessageType3.parseFromRawMessage("apple at £ 10. Add 20 p apple")).getMessageType3();
        assertEquals(new Sale("apple", new BigDecimal(10f)), messageType3.getSale());
        assertEquals(new Adjustment("apple",new BigDecimal(0.20f),AdjustmentOperation.Add), messageType3.getAdjustment());

        messageType3 = ((MessageType3Action)MessageType3.parseFromRawMessage("apple at £10. Add £20 apple")).getMessageType3();
        assertEquals(new Sale("apple", new BigDecimal(10f)), messageType3.getSale());
        assertEquals(new Adjustment("apple",new BigDecimal(20f),AdjustmentOperation.Add), messageType3.getAdjustment());

        messageType3 = ((MessageType3Action)MessageType3.parseFromRawMessage("apple at 10 p. Add 20£ apple")).getMessageType3();
        assertEquals(new Sale("apple", new BigDecimal(.10f)), messageType3.getSale());
        assertEquals(new Adjustment("apple",new BigDecimal(20f),AdjustmentOperation.Add), messageType3.getAdjustment());

    }

}
