package com.achyuthasoft.messageprocessor.message;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

public class SaleShould {

    private Sale sale;

    @Before
    public void setUp() throws Exception {
        sale=new Sale("Apples", new BigDecimal(2.0));
    }

    @Test
    public void returnTheProductType() {
        assertEquals("Apples",sale.getProductType());
    }

    @Test
    public void returnThePriceInPounds() {
        assertEquals(new BigDecimal(2.0).setScale(2, RoundingMode.HALF_UP),sale.getPriceInPounds());
    }
}
