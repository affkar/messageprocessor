package com.achyuthasoft.messageprocessor.message;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class PriceStringUtilShould {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new BigDecimal(0.10f) , "10p" }, { new BigDecimal(1.00f), "£ 1" }, { new BigDecimal(1.00f), "£1" }, { new BigDecimal(1.00f), "1£" }
        });
    }
    private BigDecimal expectedPrice;
    private String priceString;

    public PriceStringUtilShould(BigDecimal expectedPrice, String priceString) {
        this.expectedPrice = expectedPrice;
        this.priceString = priceString;
    }

    @Test
    public void getPriceFromString() {
        assertEquals(expectedPrice.setScale(2, RoundingMode.HALF_UP),PriceStringUtil.getPriceFromString(priceString));
    }
}
