package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Adjustment;
import com.achyuthasoft.messageprocessor.message.AdjustmentOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AdjustmentSummaryShould {

    AdjustmentSummary adjustmentSummary;

    @Mock
    List<Adjustment> listOfAdjustments;

    Adjustment adjustment;

    @Before
    public void setUp() throws Exception {
        adjustment=new Adjustment("apple",new BigDecimal(0.10f), AdjustmentOperation.Add);

    }

    @Test
    public void record_Adjustment() {
        adjustmentSummary=new AdjustmentSummary(listOfAdjustments);
        adjustmentSummary.record(adjustment);
        verify(listOfAdjustments, times(1)).add(adjustment);
    }

    @Test
    public void returns_TheSameAdjustmentListThatItWasConstructedWith() {
        ArrayList<Adjustment> adjustmentList = new ArrayList<>();
        adjustmentSummary=new AdjustmentSummary(adjustmentList);
        adjustmentSummary.record(adjustment);
        assertEquals(adjustmentList, adjustmentSummary.getAdjustmentList());
    }
}
