package com.achyuthasoft.messageprocessor;

import com.achyuthasoft.messageprocessor.message.Sale;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SalesManagerShould {

    SalesManager salesManager;

    @Before
    public void setUp() throws Exception {
        salesManager=new SalesManager();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void recordSale() {
        salesManager.recordSale(new Sale("apple", new BigDecimal(10.243)));
        salesManager.recordSale(new Sale("apple", new BigDecimal(10.243)));
        assertEquals(new SalesSummaryItem("apple", 2, new BigDecimal(10.243)), salesManager.getSummaryOfSales().getProductTypeSalesSummaryItemMap().get("apple"));
    }

    @Test
    public void printSummaryOfSales() {
    }

    @Test
    public void recordAdjustment() {
    }

    @Test
    public void printSummaryOfAdjustments() {
    }

    @Test
    public void getSummaryOfSales() {
    }

    @Test
    public void getSummaryOfAdjustments() {
    }
}
