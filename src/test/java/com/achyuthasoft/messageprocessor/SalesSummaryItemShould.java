package com.achyuthasoft.messageprocessor;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

public class SalesSummaryItemShould {


    SalesSummaryItem salesSummaryItem;

    @Before
    public void setup(){
        salesSummaryItem = new SalesSummaryItem("onions", 10, new BigDecimal(10.256F));
    }

    @Test
    public void getTotalMoneyRealized() {
        assertEquals(new BigDecimal(102.60f).setScale(2, RoundingMode.HALF_UP), salesSummaryItem.getTotalMoneyRealized());

    }

    @Test
    public void getPricePerItem() {
        assertEquals(new BigDecimal(10.26).setScale(2, RoundingMode.HALF_UP),salesSummaryItem.getPricePerItem());
    }
}
