package com.achyuthasoft.hamcrestmatcher;

import org.apache.commons.collections.CollectionUtils;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.internal.matchers.TypeSafeMatcher;

import java.util.List;

public class CollectionEquals<T> extends TypeSafeMatcher<List<? extends T>> {

    private final List<T> items;

    public CollectionEquals(List<T> items){
        this.items =items;
    }


    public static <T> Matcher equalTo(List<? extends T> items) {
        return new CollectionEquals(items);
    }

    @Override
    public boolean matchesSafely(List<? extends T> o) {
        return CollectionUtils.isEqualCollection(o, items);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Expected Items: - "+items);
    }

}
